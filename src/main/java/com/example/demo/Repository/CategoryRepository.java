package com.example.demo.Repository;

import com.example.demo.Entity.Category;
import com.example.demo.Entity.Handlings;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends CrudRepository<Category, Long> {

}
