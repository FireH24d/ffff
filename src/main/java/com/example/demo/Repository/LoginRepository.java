package com.example.demo.Repository;

import com.example.demo.Entity.Login;
import com.example.demo.Entity.Order;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LoginRepository extends CrudRepository<Login, Long> {
    Login findByLoginAndPassword(String log,String password);
 Login findByToken(String token);
}
