package com.example.demo.Repository;

import com.example.demo.Entity.Order;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface OrderRepository extends CrudRepository<Order, Long> {
    @Transactional
    @Modifying
    @Query(value = "insert into oorder (shop_id,total_price) values(:shop_id,:total_price)", nativeQuery = true )
    void insertorder(@Param("shop_id") long shop_id, @Param("total_price") float total_price);

}