package com.example.demo.Service;

import com.example.demo.Entity.Order;
import com.example.demo.Entity.Product;
import com.example.demo.Repository.OrderRepository;
import com.example.demo.Repository.ProductRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;
@Service
public class OrderService {
    private final OrderRepository orderRepository;

    public OrderService(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public List<Order> getAll(){
        return (List<Order>) orderRepository.findAll();
    }
    public ResponseEntity<?> getById(long id) {
        return ResponseEntity.ok(orderRepository.findById(id));
    }

    public void delete(long id){
        orderRepository.deleteById(id);
    }
    public Order update(@RequestBody Order order){
        return  orderRepository.save(order);
    }
    public void save(long shop_id,float total_price){
          orderRepository.insertorder(shop_id,total_price);
    }


}
