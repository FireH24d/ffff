package com.example.demo.Service;

import com.example.demo.Entity.Order;
import com.example.demo.Entity.OrderItem;
import com.example.demo.Repository.OrderItemRepository;
import com.example.demo.Repository.OrderRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;
@Service
public class OrderItemService {
    private final OrderItemRepository orderItemRepository;

    public OrderItemService(OrderItemRepository orderItemRepository) {
        this.orderItemRepository = orderItemRepository;
    }

    public  List<OrderItem> getAll(){
        return (List<OrderItem> )orderItemRepository.findAll();
    }
    public ResponseEntity<?> getById(long id) {
        return ResponseEntity.ok(orderItemRepository.findById(id));
    }

    public void delete(long id){
        orderItemRepository.deleteById(id);
    }
    public OrderItem update(@RequestBody OrderItem orderItem){
        return  orderItemRepository.save(orderItem);
    }
    public void save(long shop_id,long order_id,int quantity,float price){
        orderItemRepository.insertorderitem(shop_id,order_id,quantity,price);
    }
}
