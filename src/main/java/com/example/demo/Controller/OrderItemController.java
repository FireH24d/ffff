package com.example.demo.Controller;

import com.example.demo.Entity.Order;
import com.example.demo.Entity.OrderItem;
import com.example.demo.Service.OrderItemService;
import com.example.demo.Service.OrderService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
public class OrderItemController {
    private final OrderItemService orderItemService;

    public OrderItemController(OrderItemService orderItemService) {
        this.orderItemService = orderItemService;
    }

    @RequestMapping(value="/orderItem",method= RequestMethod.GET, headers = "Accept=application/json")
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(orderItemService.getAll());
    }
    @RequestMapping(value="/orderItem/{id}", method=RequestMethod.GET, headers = "Accept=application/json")
    public ResponseEntity<?> get(@PathVariable long id){
        return ResponseEntity.ok(orderItemService.getById(id));}
    @RequestMapping(value = "/orderItem/{id}", method = RequestMethod.DELETE, headers = "Accept=application/json")
    public void delete(@PathVariable int id){
        orderItemService.delete(id);
    }
    @RequestMapping(value="/orderItemm",method=RequestMethod.POST, headers = "Accept=application/json")
    public ResponseEntity<?> update(@RequestBody OrderItem orderItem){
        return ResponseEntity.ok(orderItemService.update(orderItem));
    }
    @PostMapping(value="/orderItem/create")
    public void save(@RequestHeader("shop_id") long shop_id,@RequestHeader("order_id") long order_id,@RequestHeader("quantity") int quantity,@RequestHeader("price") float price){
        orderItemService.save( shop_id, order_id, quantity, price);
    }
}
