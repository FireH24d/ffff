package com.example.demo.Controller;

import com.example.demo.Entity.Order;
import com.example.demo.Service.OrderService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class OrderController {
    private final OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @RequestMapping(value="/order",method= RequestMethod.GET, headers = "Accept=application/json")
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(orderService.getAll());

    }
    @RequestMapping(value="/order/{id}", method=RequestMethod.GET, headers = "Accept=application/json")
    public ResponseEntity<?> get(@PathVariable long id){
        return ResponseEntity.ok(orderService.getById(id));}
    @RequestMapping(value = "/order/{id}", method = RequestMethod.DELETE, headers = "Accept=application/json")
    public void delete(@PathVariable int id){
        orderService.delete(id);
    }
    @RequestMapping(value="/order",method=RequestMethod.POST, headers = "Accept=application/json")
    public ResponseEntity<?> update(@RequestBody Order order){
        return ResponseEntity.ok(orderService.update(order));
    }
    @PostMapping(value="/ordder")
    public void save(@RequestHeader("shop_id") long shop_id,@RequestHeader("total_price") float total_price){
         orderService.save(shop_id,total_price);
    }


}
