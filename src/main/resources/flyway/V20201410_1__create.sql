CREATE TABLE if not exists "handlings" (
                             "id" serial NOT NULL,
                             "factory_id" int8 NOT NULL,
                             "order_id" int8 NOT NULL,
                             "delivery_id" int8 NOT NULL,
                             "date" DATE NOT NULL,
                             "status" varchar(255) NOT NULL) WITH (
      OIDS=FALSE
    );



CREATE TABLE if not exists "factory" (
                           "id" serial NOT NULL,
                           "name" varchar(255) NOT NULL,
                           "address" varchar(255) NOT NULL,
                           "contact_number" varchar(255) NOT NULL) WITH (
      OIDS=FALSE
    );



CREATE TABLE if not exists "delivery" (
                            "id" serial NOT NULL,
                            "driver_id" int8 NOT NULL,
                            "date" DATE NOT NULL) WITH (
      OIDS=FALSE
    );



CREATE TABLE if not exists "driver" (
                          "id" serial NOT NULL,
                          "name" varchar(255) NOT NULL,
                          "surname" varchar(255) NOT NULL,
                          "gender" varchar(255) NOT NULL) WITH (
      OIDS=FALSE
    );



CREATE TABLE if not exists "category" (
                            "id" serial NOT NULL,
                            "name" varchar(255) NOT NULL,
                            "description" TEXT NOT NULL) WITH (
      OIDS=FALSE
    );



CREATE TABLE if not exists "product" (
                           "id" serial NOT NULL,
                           "category_id" int8 NOT NULL,
                           "name" varchar(255) NOT NULL,
                           "price" float8 NOT NULL,
                           "description" TEXT NOT NULL) WITH (
      OIDS=FALSE
    );



CREATE TABLE if not exists "order_item" (
                              "id" serial NOT NULL,
                              "order_id" int8 NOT NULL,
                              "product_id" int8 NOT NULL,
                              "quantity" int NOT NULL,
                              "price" float8 NOT NULL

) WITH (
      OIDS=FALSE
    );



CREATE TABLE if not exists "oorder" (
                         "id" serial NOT NULL,
                         "shop_id" int8 NOT NULL,
                         "total_price" float8 NOT NULL
) WITH (
      OIDS=FALSE
    );



CREATE TABLE if not exists "login" (
                         "id" serial NOT NULL,
                         "shop_id" int8 NOT NULL,
                         "login" varchar(255) NOT NULL,
                         "password" varchar(255) NOT NULL,
                         "token" varchar(255) NOT NULL

) WITH (
      OIDS=FALSE
    );



CREATE TABLE if not exists "shop" (
                        "id" serial NOT NULL,
                        "name" varchar(255) NOT NULL,
                        "address" varchar(255) NOT NULL,
                        "phone" varchar(11) NOT NULL
) WITH (
      OIDS=FALSE
    );



CREATE TABLE if not exists "payment" (
                           "id" serial NOT NULL,
                           "shop_id" int8 NOT NULL,
                           "date" DATE NOT NULL,
                           "amount_paid" float8 NOT NULL
) WITH (
      OIDS=FALSE
    );


