ALTER TABLE "handlings" ADD CONSTRAINT "handlings_pk" PRIMARY KEY ("id");
ALTER TABLE "factory" ADD CONSTRAINT "factory_pk" PRIMARY KEY ("id");
ALTER TABLE "delivery" ADD CONSTRAINT "delivery_pk" PRIMARY KEY ("id");
ALTER TABLE "driver" ADD CONSTRAINT "driver_pk" PRIMARY KEY ("id");
ALTER TABLE "category" ADD CONSTRAINT "category_pk" PRIMARY KEY ("id");
ALTER TABLE "product" ADD CONSTRAINT "product_pk" PRIMARY KEY ("id");
ALTER TABLE "oorder" ADD CONSTRAINT "order_pk" PRIMARY KEY ("id");
ALTER TABLE "login" ADD CONSTRAINT "login_pk" PRIMARY KEY ("id");
ALTER TABLE "shop" ADD CONSTRAINT "shop_pk" PRIMARY KEY ("id");
ALTER TABLE "payment" ADD CONSTRAINT "payment_pk" PRIMARY KEY ("id");
ALTER TABLE "order_item" ADD CONSTRAINT "order_item_pk" PRIMARY KEY ("id");

